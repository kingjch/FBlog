
var express = require('express');
var path = require('path');
var webpack = require('webpack');
var history = require('connect-history-api-fallback');

var config = require('../config/webpack-config.js');
var webpackConfig = require('./webpack.client.config.js');

var port = config.dev.port || 3000;

var app = express();
var compiler = webpack(webpackConfig);

var devMiddleware = require('webpack-dev-middleware')(compiler, {
	quiet: true
});

var hotMiddleware = require('webpack-hot-middleware')(compiler, {
	log: () => {},
	heartbeat: 2000
});

compiler.plugin('compilation', function(compilation) {
	compilation.plugin('html-webpack-plugin-after-emit', function(data, cb) {
		hotMiddleware.publish({action: 'reload'});
		cb && cb();
	})
});

app.use(history());
app.use(devMiddleware);
app.use(hotMiddleware);
app.use('/static', express.static('./static'));

var server = app.listen(port);
console.log(`app is listenning on port:${port}`)


