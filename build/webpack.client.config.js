
var webpack = require('webpack');
var merge = require('webpack-merge');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');

var baseConfig = require('./webpack.base.config.js');

Object.keys(baseConfig.entry).forEach(function(name) {
	baseConfig.entry[name] = ['webpack-hot-middleware/client?noInfo=false', baseConfig.entry[name]]
});

module.exports = merge(baseConfig, {
	devtool: '#cheap-module-eval-source-map',
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new FriendlyErrorsPlugin(),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: './src/index.html',
			inject: true
		})
	]
})