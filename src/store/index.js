
import Vue from 'vue'
import Vuex from 'vuex'

import moduleTest1 from './modules/test1'
Vue.use(Vuex);

export function createStore() {
	return new Vuex.Store({
		state: {
			articleList: [1,2]
		},
		getters: {
			filterArticle: state => {
				return state.articleList.concat(12);
			}
		},
		mutations: {
			addArticle(state) {
				state.articleList.push(4)
			},
			// loadMoreArticle(state, articles) {
			// 	console.log(state)
			// }
		},
		actions: {
			addArticle({ commit }) {
				setTimeout(function() {
					commit('addArticle')
				}, 2000)
				
			}
		},
		modules: {
			moduleTest1
		}
	})
}