
import FMenu from './Menu'

FMenu.install = function(Vue) {
	Vue.component(FMenu.name, FMenu)
}

export default FMenu