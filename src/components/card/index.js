
import FCard from './Card'

FCard.install = function(Vue) {
	Vue.component(FCard.name, FCard)
}

export default FCard