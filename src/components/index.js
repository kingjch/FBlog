
import Vue from 'vue'

import Input from './input/index'
import Meun from './menu/index'
import Card from './card/index'
import Row from './row/index'
import Col from './col/index'

const components = [
	Input,
	Meun,
	Card,
	Row,
	Col
]
const install = function(Vue, opt = {}) {
	components.forEach(component => {
		component.install(Vue);
	})
}

install(Vue);