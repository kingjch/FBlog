
import FCol from './Col'

FCol.install = function(Vue) {
	Vue.component(FCol.name, FCol)
}

export default FCol