import './col.css'
export default {
	name: 'f-col',
	props: {
		tag: {
			type: String,
			default: 'div'
		},

	},

	computed: {
		gutter() {
			let _parent = this.$parent;
			while(_parent && _parent.$options.componentName === 'f-row') {
				_parent = parent.$parent;
			}
			return _parent ? _parent.gutter : 0;
		}
	},
	render(h) {
		let style = {};
		if(this.gutter) {
			style.paddingLeft = this.gutter / 2 + 'px';
			style.paddingRight = style.paddingLeft;
		}

		return h(this.tag, {
			class: ['f-col'],
			style: style
		}, this.$slots.default)
	}
}

