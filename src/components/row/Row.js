import './row.css'
export default {
	name: 'f-row',
	props: {
		tag: {
			type: String,
			default: 'div'
		},
		gutter: Number
	},
	computed: {
		style() {
			let style = {};
			if(this.gutter) {
				style.marginLeft = this.gutter / 2 + 'px';
				style.marginRight = style.marginLeft;
			}
			return style
		}
	},
	render(h) {
		return h(this.tag, {
			class: ['f-row'],
			style: this.style
		}, this.$slots.default)
	}
}