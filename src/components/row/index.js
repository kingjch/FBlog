
import FRow from './Row'

FRow.install = function(Vue) {
	Vue.component(FRow.name, FRow)
}

export default FRow