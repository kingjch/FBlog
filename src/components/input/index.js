
import FInput from './Input'

FInput.install = function(Vue) {
	Vue.component(FInput.name, FInput)
}

export default FInput;
