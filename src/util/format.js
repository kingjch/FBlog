
const formatTm = (date = new Date()) => {
	var yy = date.getFullYear(),
		mm = date.getMonth(),
		dd = date.getDay();

	return `${yy}-${mm}-${dd}`
}

export default {
	formatTm
}