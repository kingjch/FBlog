
import Vue from 'vue'
import App from './App'
import Vuex from 'vuex'
import { createRouters } from '@/routers/index'
import { createStore } from '@/store/index'
const router = createRouters();
const store = createStore();

new Vue({
	el: '#app',
	router,
	store,
	template: '<App />',
	components: { App }
})