
import Vue from 'vue'
import Router from 'vue-router'

import Test1 from '@/views/Test1'
import Test2 from '@/views/Test2'
Vue.use(Router);

export function createRouters() {
	return new Router({
		mode: 'history',
		routes: [
			{
				path: '/test1',
				component: Test1
			},
			{
				path: '/test2',
				component: Test2
			}
		]
	})
}